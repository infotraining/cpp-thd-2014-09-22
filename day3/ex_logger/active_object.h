#ifndef ACTIVE_OBJECT_H
#define ACTIVE_OBJECT_H

#include <future>
#include <thread>
#include <functional>
#include "../../day2/thread_safe_queue/thread_safe_queue.h"

typedef std::function<void()> task_t;

class active_object
{
    thread_safe_queue<task_t> q;
    std::thread servant;
public:
    active_object() : q(10)
    {
        servant = std::thread( [this] ()
            {
                for(;;)
                {
                    task_t t;
                    q.pop(t);
                    if(!t) return;
                    t();
                }
            }
        );
    }

    void send(task_t task)
    {
        q.push(task);
    }

    template<typename F>
    auto send(F f) -> std::future<decltype(f())>
    {
        auto task = std::make_shared<std::packaged_task< decltype(f())() >>(f);
        //auto task = std::make_shared<std::packaged_task<typename std::result_of<F()>::type()>> (f);
        auto res = task->get_future();
        q.push([task] () { (*task)();});
        return res;
    }

    ~active_object()
    {
        q.push(nullptr);
        servant.join();
    }
};

#endif // ACTIVE_OBJECT_H
