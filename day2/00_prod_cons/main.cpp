#include <iostream>
#include <queue>
#include <thread>
#include <vector>
#include <mutex>
#include <condition_variable>

using namespace std;

queue<int> q;
mutex qmtx;
condition_variable cond;

void producer()
{
    for (int i = 0 ; i < 100 ; ++i)
    {
        {
            lock_guard<mutex> l(qmtx);
            q.push(i);
            cond.notify_one();
        }
        this_thread::sleep_for(200ms);
    }
}

void consumer(int id)
{
    for(;;)
    {
        unique_lock<mutex> l(qmtx);
//        while (q.empty())
//        {
//            cond.wait(l);
//        }
        cond.wait(l, [] () { return !q.empty(); });
        int msg = q.front();
        q.pop();
        cout << id << " got: " << msg << endl;
    }
}

int main()
{
    cout << "Prod Cons!" << endl;
    vector<thread> thds;
    thds.emplace_back(producer);
    thds.emplace_back(consumer, 1);
    thds.emplace_back(consumer, 2);
    thds.emplace_back(consumer, 3);
    thds.emplace_back(consumer, 4);
    for(auto& th : thds) th.join();
    return 0;
}

