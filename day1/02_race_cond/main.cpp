#include <iostream>
#include <thread>
#include <vector>
#include <atomic>
#include <mutex>

using namespace std;
mutex mtx;
long counter = 0;

//atomic<long> counter(0);

class LockGuard
{
    mutex& mtx;
public:
    LockGuard(mutex& m) : mtx(m)
    {
        mtx.lock();
    }

    ~LockGuard()
    {
        mtx.unlock();
    }

};

void increment()
{
    for (int i = 0 ; i < 100000 ; ++i)
    {
        unique_lock<mutex> l(mtx);
        ++counter;
        if (counter == 1000) return;
    }
}

int main()
{
    cout << "Counter!" << endl;

    auto start = chrono::high_resolution_clock::now();

    vector<thread> thds;
    for (int i = 0 ; i < 4 ; ++i)
    {
        thds.emplace_back(increment);
    }
    for(auto &th : thds) th.join();


    auto end = chrono::high_resolution_clock::now();
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;

    cout << "Counter = " << counter << endl;
    return 0;
}

