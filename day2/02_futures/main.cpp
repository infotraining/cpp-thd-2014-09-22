#include <iostream>
#include <thread>
#include <vector>
#include <future>

using namespace std;

int question()
{
    cout << "Question id = " << this_thread::get_id() << endl;
    this_thread::sleep_for(chrono::seconds(5));

    return 42;
}

int main()
{
    cout << "Hello World!" << endl;
    cout << "Thread id = " << this_thread::get_id() << endl;
    //future<int> res = async(launch::async, question);
    packaged_task<int()> pt(question);
    future<int> res = pt.get_future();
    thread th(move(pt));
    th.detach();

    cout << "Answer = " << res.get() << endl;
    return 0;
}

