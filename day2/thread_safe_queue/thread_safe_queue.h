#ifndef THREAD_SAFE_QUEUE_H
#define THREAD_SAFE_QUEUE_H

#include <queue>
#include <mutex>
#include <condition_variable>

template <typename T>
class thread_safe_queue
{
    std::queue<T> q;
    std::mutex qmtx;
    std::condition_variable cond_out;
    std::condition_variable cond_in;
    size_t max_size;
public:
    thread_safe_queue(size_t max_size) :
        max_size(max_size)
    {

    }

    void push(const T& item)
    {
        std::unique_lock<std::mutex> l(qmtx);
        while (q.size() >= max_size)
        {

            cond_in.wait(l);
        }
        q.push(item);
        cond_out.notify_one();
    }

    void push(T&& item)
    {
        std::unique_lock<std::mutex> l(qmtx);
        while (q.size() >= max_size)
        {
            cond_in.wait(l);
        }
        q.push(std::move(item));
        cond_out.notify_one();
    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> l(qmtx);
        while (q.empty())
        {
            cond_out.wait(l);
        }
        item = std::move(q.front());
        q.pop();
        cond_in.notify_one();
    }

    bool pop_nowait(T& item)
    {
        std::lock_guard<std::mutex> l(qmtx);
        if (q.empty())
        {
            return false;
        }
        item = std::move(q.front());
        q.pop();
        cond_in.notify_one();
        return true;
    }
};

#endif // THREAD_SAFE_QUEUE_H
