## Multithreading programming in C++

### Additonal information

* login and password for VM:

```
dev  /  tymczasowe
```

* reinstall VBox addon

```
sudo /etc/init.d/vboxadd setup
```

* proxy settings

We can add them to `.profile`

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=https://10.144.1.10:8080
```

* git

```
git clone https://bitbucket.org/infotraining/cpp-thd-2014-09-22

git-cheat-sheet
http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf

git status  # local repository status
git pull    # getting files from repository
git stash   # moving modifications to stash
```

## Why is processing a sorted array faster than an unsorted array?

http://stackoverflow.com/questions/11227809/why-is-processing-a-sorted-array-faster-than-an-unsorted-array