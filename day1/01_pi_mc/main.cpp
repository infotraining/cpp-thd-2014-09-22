#include <iostream>
#include <random>
#include <chrono>
#include <vector>
#include <thread>
#include <atomic>

using namespace std;

atomic<long> counter(0);

long calc_hits(long N)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(-1, 1);


    for (int n = 0 ; n < N ; ++n)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
            counter++;
    }
    return counter;
}



int main()
{
    long N = 100'000'000;
    cout << "Pi calc!" << endl;
    auto start = chrono::high_resolution_clock::now();
    cout << double(calc_hits(N)/double(N))*4 << endl;
    auto end = chrono::high_resolution_clock::now();
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;

    vector<thread> thds;
    int n_of_threads = thread::hardware_concurrency();

    vector<long> res(n_of_threads);

    start = chrono::high_resolution_clock::now();
    counter = 0;
    for (int i = 0 ; i < n_of_threads ; ++i)
    {
        thds.emplace_back( [i, &res, N, n_of_threads] () {
            res[i] = calc_hits(N/n_of_threads);
        } );
    }
    for (auto& th : thds) th.join();

    //cout << double(accumulate(res.begin(), res.end(), 0L)/double(N))*4 << endl;
    cout << double(counter)/double(N)*4 << endl;
    end = chrono::high_resolution_clock::now();
    cout << "Elapsed parallel = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;

    return 0;
}

