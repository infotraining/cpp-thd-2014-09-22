#include <iostream>
#include <vector>
#include <thread>
#include <future>
#include <algorithm>
#include <numeric>

using namespace std;

template<typename It, typename T>
T parallel_accumulate(It begin, It end, T init)
{
    // parallel accumulate using future async and accumulate
    int N = thread::hardware_concurrency();
    vector<future<T>> fres;

    size_t block_size = distance(begin, end)/N;

    It block_start = begin;
    for (int i = 0 ; i < N ; ++i)
    {
        It block_end = block_start;
        advance(block_end, block_size);
        if (i == N-1) block_end = end;
        fres.push_back(async(launch::async,
                             accumulate<It, T>, block_start, block_end, init));
        block_start = block_end;
    }

    T res{};
    for (auto& fut : fres) res += fut.get();
    return res;
}

int main()
{
    cout << "Accumulate" << endl;

    vector<long> v1;
    for (long i = 0 ; i < 1000'000'000 ; ++i)
    {
        v1.push_back(i);
    }

    auto time_start = chrono::high_resolution_clock::now();
    long res = accumulate(v1.begin(), v1.end(), 0L);
    auto time_end = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::microseconds>(time_end-time_start).count();
    cout << " um - serial" << endl;
    cout << "Res = " << res << endl;

    time_start = chrono::high_resolution_clock::now();
    res = parallel_accumulate(v1.begin(), v1.end(), 0L);
    time_end = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::microseconds>(time_end-time_start).count();
    cout << " um  - parallel" << endl;
    cout << "Res = " << res << endl;

    return 0;
}

