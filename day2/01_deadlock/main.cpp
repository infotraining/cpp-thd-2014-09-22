#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <condition_variable>

using namespace std;

class BankAccount
{
    int id;
    double balance;
    mutex mtx;

public:
    BankAccount(int id, double balance) :
        id(id), balance(balance)
    {

    }

    void print()
    {
        cout << "Account " << id << " : " << balance << endl;
    }

    void transfer(BankAccount& to, double amount)
    {
        unique_lock<mutex> l1(mtx, defer_lock);
        unique_lock<mutex> l2(to.mtx, defer_lock);
        lock(l1, l2);
        balance -= amount;
        to.balance += amount;
    }
};

void test_massive_transfer(BankAccount& from, BankAccount& to)
{
    for (int i = 0 ; i < 1000000 ; ++i)
    {
        from.transfer(to, 1.0);
    }
}

int main()
{
    cout << "Bank Account" << endl;
    BankAccount one(1, 2000000);
    BankAccount two(2, 2000000);
    thread th1(test_massive_transfer, ref(one), ref(two));
    thread th2(test_massive_transfer, ref(two), ref(one));
    th1.join();
    th2.join();
    one.print();
    two.print();
    return 0;
}

