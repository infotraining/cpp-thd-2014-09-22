#include <iostream>
#include <queue>
#include <thread>
#include <vector>
#include <mutex>
#include <condition_variable>

#include "thread_safe_queue.h"

using namespace std;

thread_safe_queue<long> q(1000);
mutex qmtx;
condition_variable cond;

void producer()
{
    for (long i = 100000000 ; i < 100001000 ; ++i)
    {
        q.push(i);
        //this_thread::sleep_for(200ms);
    }
    q.push(-1);
}

bool is_prime(long num)
{
    bool is_prime = true;
    for (long div = 2 ; div < num ; ++div)
    {
        if (num % div == 0)
        {
            is_prime = false;
            break;
        }

    }
    return is_prime;
}

void consumer(int id)
{
    for(;;)
    {
        long num;
        q.pop(num);
        if (num == -1)
        {
            q.push(-1);
            return;
        }

        if (is_prime(num))
        {
            cout << num << endl;
        }
    }
}

int main()
{
    cout << "Prod Cons!" << endl;
    vector<thread> thds;
    thds.emplace_back(producer);
    thds.emplace_back(consumer, 1);
    thds.emplace_back(consumer, 2);
    thds.emplace_back(consumer, 3);
    thds.emplace_back(consumer, 4);
    for(auto& th : thds) th.join();
    return 0;
}

