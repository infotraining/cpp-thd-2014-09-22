#include <iostream>
#include <functional>
#include <future>
#include "thread_pool.h"

/*
class Runnable
{
public:
    virtual void run();
};

queue<Runnable*>

void worker()
{
    for(;;)
    {
        Runnable* task;
        q.pop(task);
        task->run();
    }
}*/

using namespace std;

void task()
{
    cout << "Hello from task" << endl;
}

int question()
{
    cout << "Question id = " << this_thread::get_id() << endl;
    this_thread::sleep_for(chrono::seconds(2));

    return 42;
}

int main()
{
    thread_pool tp(4);
    tp.add_task(task);
    for (int i = 0 ; i < 10 ; ++i)
    {
        tp.add_task([i] () { cout << "Hello from lambda " << i << endl; });
    }
    tp.add_task([&tp] { tp.add_task(task);});
    //future<int> res = async(launch::async, question);
    future<int> res = tp.async(&question);
    cout << "RES = " << res.get() << endl;
    return 0;
}

