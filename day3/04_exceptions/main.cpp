#include <iostream>
#include <thread>
#include <stdexcept>
#include <exception>
#include <string>
#include <vector>

using namespace std;

void may_throw(int id, int arg)
{
    if (arg == 13)
        throw runtime_error("Error in may_throw " + to_string(id));
}

void worker(int id, exception_ptr& eptr)
{
    try
    {
        for (int i = 0 ; i < 50 ; ++i)
        {
            cout << "THD " << id << " : " << i << endl;
            may_throw(id, i);
        }
    }
    catch(...)
    {
        cout << "Catch in " << id << endl;
        eptr = std::current_exception();
    }
}

int main()
{
    vector<thread> thds(2);
    vector<exception_ptr> eptrs(2);
    for (int i = 0 ; i < 2 ; ++i)
    {
        thds[i] = thread(worker, i, ref(eptrs[i]));
    }
    for (auto& th : thds) th.join();

    for (exception_ptr& eptr : eptrs)
    {
        if(eptr)
        {
            try
            {
                rethrow_exception(eptr);
            }
            catch(const runtime_error& e)
            {
                cout << "There was exception ";
                cout << e.what() << endl;
            }
        }
    }
    return 0;
}

