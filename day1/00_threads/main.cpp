#include <iostream>
#include <thread>
#include <vector>

/* https://bitbucket.org/infotraining/cpp-thd-2014-09-22 */
/* leszek.tarkowski@infotraining.pl */

using namespace std;

void hello(int id)
{
    cout << "Hello from thread " << id << endl;
    this_thread::sleep_for(1s);
}

struct Functor
{
    void operator()(int id)
    {
        cout << "From functor " << id << endl;
    }
};

void print_vector(vector<int>& v)
{
    for (int &el : v)
        cout << el << endl;
}

class Buffer
{
    vector<int> buff_;
public:
    void assign(const vector<int>& source)
    {
        buff_.assign(source.begin(), source.end());
    }

    std::vector<int> data() const
    {
        return buff_;
    }
};

thread generate_thread()
{
    vector<int> v{9,8,7,6,5,4,3,2,1};
    thread temp(print_vector, ref(v));
    return temp;
}

int main()
{
    cout << "Threads!" << endl;
    vector<thread> threads;
    threads.emplace_back(&hello, 1);
    Functor f;
    threads.emplace_back(f, 2);
    threads.emplace_back( [] () { cout << "From lambda" << endl;});

    vector<int> v{1,2,3,4};
    threads.emplace_back( print_vector, ref(v) );

    Buffer buff;

    threads.emplace_back( bind(&Buffer::assign, ref(buff), cref(v)));
    threads.emplace_back( [&]() {buff.assign(v);});

    //thread th = generate_thread();
    threads.push_back(generate_thread());

    for (thread& th : threads) th.join();
    cout << "After join" << endl;

    return 0;
}

