#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <atomic>

using namespace std;

template <typename T>
struct node
{
    node* prev;
    T data;
    node (T data, node* prev) : data(data), prev(prev)
    {}
};

template <typename T>
class stack
{
    mutex mtx;
    node<T> *head;
public:
    stack() : head(nullptr)
    {
    }

    void push(T item)
    {
        lock_guard<mutex> l(mtx);
        node<T>* new_node = new node<T>(item, head);
        head = new_node;
    }

    T pop()
    {
        node<T>* old_node;
        {
            lock_guard<mutex> l(mtx);
            old_node = head;
            head = old_node->prev;
        }
        T res = old_node->data;
        delete old_node;
        return res;
    }

};

template <typename T>
class atomic_stack
{
    atomic<node<T>*> head;

public:
    atomic_stack() : head(nullptr)
    {
    }

    void push(T item)
    {
        node<T>* new_node = new node<T>(item, head.load());
        //head.store(new_node); <-- still race..
        while(!head.compare_exchange_weak(new_node->prev, new_node));
    }

    T pop()
    {
        node<T>* old_node = head.load();
        while(old_node && !head.compare_exchange_weak(old_node, old_node->prev));
        if (old_node)
        {
            T res = old_node->data;
            //delete old_node;
            return res;
        }
    }

};

void push_and_pop(atomic_stack<int>& s, int start)
{
    for (int i = start ; i < start + 100 ; ++i)
        s.push(i);

    for (int i = start ; i < start + 100 ; ++i)
        cout << s.pop() << " ; ";
    cout << endl;
}

int main()
{
    cout << "Hello World!" << endl;
    atomic_stack<int> s;
    vector<thread> thds;
    thds.emplace_back(push_and_pop, ref(s), 0);
    thds.emplace_back(push_and_pop, ref(s), 100);
    for (auto& th : thds) th.join();
    return 0;
}

