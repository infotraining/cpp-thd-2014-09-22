#ifndef THREAD_POOL_H
#define THREAD_POOL_H
#include <functional>
#include <thread>
#include <vector>
#include <future>

#include "../../day2/thread_safe_queue/thread_safe_queue.h"

typedef std::function<void()> task_t;

class thread_pool
{
    std::vector<std::thread> workers;
    thread_safe_queue<task_t> q;


public:
    thread_pool(int n_of_workers) : q(10)
    {
        for (int i = 0 ; i < n_of_workers ; ++i)
            workers.emplace_back(&thread_pool::worker, this);
    }

    void add_task(task_t task)
    {
        q.push(task);
    }

    template<typename F>
    auto async(F f) -> std::future<decltype(f())>
    {
        auto task = std::make_shared<std::packaged_task< decltype(f())() >>(f);
        //auto task = std::make_shared<std::packaged_task<typename std::result_of<F()>::type()>> (f);
        auto res = task->get_future();
        q.push([task] () { (*task)();});
        return res;
    }

    void worker()
    {
        for(;;)
        {
            task_t task;
            q.pop(task);
            if (!task) return;
            task();
        }
    }

    ~thread_pool()
    {
        for(auto& th : workers) q.push(nullptr);
        for(auto& th : workers) th.join();
    }
};

#endif // THREAD_POOL_H
